###############################################################################
# 
# Copyright (c) 2005- Shinji Kashihara. All rights reserved.
# This program and the accompanying materials except for /lib directory are
# made available under the terms of the Eclipse Public License v1.0 which
# accompanies this distribution, and is available at /epl-v10.html.
#
###############################################################################
#
# Pleiades プラグイン別辞書プロパティーについて
#
###############################################################################

このディレクトリーの *.properties は translation.properties の元となる
プロパティー・ファイル群です。分かりやすくするためにファイル名はプラグイン別に
なっていますが、辞書的に意味はありません。

translation.properties へのマージは
jp.sourceforge.mergedoc.pleiades.generator.Generator
により行われます。

通常は意識する必要はありませんが、プロパティーのキー先頭に特殊な識別子を
指定することで、特別な処理を行うことが可能になります。
使用可能な識別子を下記に示します。



■%REGEX%

  プロパティーのキー (原文) に正規表現を使用可能にします。
  識別子を除去し、translation-regex.properties に出力されます。

  ・キーの先頭に %REGEX% を付加
  ・正規表現の \ は \\ と記述
  
    例）空白以外              (\S+) → (\\S+)
        特殊文字のエスケープ  \(    → \\(


■%EXCLUDE%

  特定の Java パッケージで特定の訳を除外可能にします。
  これは実行時のスタックとレース情報を使用するため、辞書を静的に利用する場合は
  意味を持ちません。
  識別子を除去し、translation-exclude.properties に出力されます。

  ・英文の先頭に %EXCLUDE% を付加し、パッケージ接頭辞または完全クラス名を指定
  ・パッケージはカンマ区切りで複数指定可能

    例）%EXCLUDE%eee=com.foo.xxx,com.bar.XClass
