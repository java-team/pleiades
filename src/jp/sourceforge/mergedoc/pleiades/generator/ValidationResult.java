/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.generator;

/**
 * 検証結果クラスです。
 * <p>
 * @author cypher256
 */
public class ValidationResult {

	/** エラー数 */
	protected int errorCount;

	/** 警告数 */
	protected int warnCount;
	
	/**
	 * 成功か判定します。
	 * @return 成功の場合は true
	 */
	public boolean isSuccess() {
		return errorCount == 0 && warnCount == 0;
	}
}
