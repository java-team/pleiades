/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.generator.nls;

import java.io.File;

/**
 * フィーチャー・リソースを保持するクラスです。
 * <p>
 * @author cypher256
 */
public class Feature extends Plugin {

	/**
	 * フィーチャーを構築します。
	 * <p>
	 * @param featureFile フィーチャー・フォルダーまたはファイル
	 */
	public Feature(File featureFile) {
		super(featureFile);
	}

	/**
	 * 指定されたフォルダーのリソースをロードします。
	 * <p>
	 * @param featureFolder フィーチャー・フォルダー
	 */
	@Override
	protected void loadFolderPlugin(File featureFolder) {

		setIdInFileName(featureFolder);

		getEnPropertySet().load(new File(featureFolder, "feature.properties"));
		getJaPropertySet().load(new File(featureFolder, "feature_ja.properties"));
		getJaPropertySet().load(new File(featureFolder, "feature_ja_JP.properties"));
	}
}
