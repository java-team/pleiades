/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.generator.nls;

import java.io.IOException;

import jp.sourceforge.mergedoc.pleiades.generator.Validator;
import jp.sourceforge.mergedoc.pleiades.resource.Files;

/**
 * 言語パックから、ヘルプに関する日本語訳を抽出し、Pleiades 形式の
 * プロパティー・ファイルに出力するクラスです。
 * <p>
 * @author cypher256
 */
public class J10nHelpExtractor extends HelpExtractor {
	
	/**
	 * 抽出を開始するための main メソッドです。
	 * <p>
	 * @param args NLS 取得元プラグイン確認デバッグ・ログ出力用の英語リソース文字列
	 * @throws IOException 入出力例外が発生した場合
	 */
	public static void main(String... args) throws IOException {
		inNlsFolder = Files.conf("props/temp/help.test.nls");
		new J10nHelpExtractor().run("help", args);
	}
	
	/**
	 * 事前バリデーターを作成します。
	 * @param logFileName ログ・ファイル名。例) "props/validator.log"
	 */
	@Override
	protected Validator createBeforeValidator(String logFileName) {
		return new J10nHelpValidator(logFileName);
	}
}
