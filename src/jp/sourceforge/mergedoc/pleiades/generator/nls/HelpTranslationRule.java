/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.generator.nls;

import static jp.sourceforge.mergedoc.pleiades.resource.FileNames.*;

import jp.sourceforge.mergedoc.pleiades.generator.TranslationRule;
import jp.sourceforge.mergedoc.pleiades.resource.Property;
import jp.sourceforge.mergedoc.pleiades.resource.PropertySet;

/**
 * ヘルプ翻訳ルール・クラスです。
 * <p>
 * @author cypher256
 */
public class HelpTranslationRule extends TranslationRule {

	/** ヘルプ上書きプロパティー */
	protected static final PropertySet vHelpOverrideProp = new PropertySet(VALID_HELP_OVERRIDE_PROP);

	/**
	 * ヘルプ翻訳ルールを構築します。
	 * <p>
	 * @param logFileName ログ・ファイル名。例) "props/validator.log"
	 * @param existsProps 重複チェックに使用する既存プロパティー
	 */
	public HelpTranslationRule(String logFileName, PropertySet... existsProps) {

		super(logFileName, existsProps);
	}

	/**
	 * プロパティーを句点解析により、。や . などで文を分割した後、
	 * 翻訳ルールを適用します。
	 * 分割されたエントリー分、数が増加します。
	 * <p>
	 * デフォルトの TranslationRule の処理に加え、ヘルプ校正用の
	 * 上書きプロパティーで上書きします。
	 * <p>
	 * @param inProp プロパティー・セット
	 * @return ルール適用後のプロパティー
	 */
	@Override
	public PropertySet apply(PropertySet inProp) {

		PropertySet outProp = super.apply(inProp);

		// 上書き
		for (Property p : vHelpOverrideProp) {

			String jaOld = outProp.get(p.key);
			if (jaOld == null) {
				log.error("ヘルプ上書きプロパティーに抽出したヘルプに" +
					"存在しないキーが定義されています。\n%s", p);
			} else if (jaOld.equals(p.value)) {
				log.error(	"ヘルプ上書きプロパティーに抽出したヘルプと" +
					"まったく同一のエントリーが定義されています。\n%s", p);
			} else {
				outProp.put(p);
			}
		}

		return outProp;
	}
}
