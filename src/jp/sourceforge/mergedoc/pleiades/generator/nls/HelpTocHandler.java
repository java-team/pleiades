/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.generator.nls;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import jp.sourceforge.mergedoc.pleiades.log.Logger;
import jp.sourceforge.mergedoc.pleiades.resource.PropertySet;

/**
 * ヘルプの toc や topic XML をパースするハンドラーです。
 * <p>
 * @author cypher256
 */
public class HelpTocHandler extends DefaultHandler {

	/** ロガー */
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(HelpTocHandler.class);

	/** 処理結果を格納するプロパティー */
	private final PropertySet prop;

	/**
	 * ヘルプ toc ハンドラーを構築します。
	 * <p>
	 * @param prop 処理結果を格納するプロパティー
	 */
	public HelpTocHandler(PropertySet prop) {
		this.prop = prop;
	}

	/**
	 * XML 要素開始時の処理です。
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		if (qName.equals("toc")) {
			
			String value = attributes.getValue("label");
			prop.put("toc", value);
			
		} else if (qName.equals("topic")) {
			
			String key = attributes.getValue("href");
			String value = attributes.getValue("label");
			prop.put(key, value);
		}
	}
}
