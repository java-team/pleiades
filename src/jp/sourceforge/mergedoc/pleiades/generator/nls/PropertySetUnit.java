/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.generator.nls;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import jp.sourceforge.mergedoc.pleiades.resource.PropertySet;

/**
 * 英語と日本語のプロパティーを対で保持するクラスです。
 * <p>
 * @author cypher256
 */
public class PropertySetUnit {

	/** 英語プロパティー */
	private PropertySet enProperties = new IgnoreFileNotFoundProperties();

	/** 日本語プロパティー */
	private PropertySet jaProperties = new IgnoreFileNotFoundProperties();
	
	/** ファイルが無い場合、無視するプロパティー */
	private static class IgnoreFileNotFoundProperties extends PropertySet {
		
		private static final long serialVersionUID = 1L;

		@Override
		protected InputStream getInputStream(File file) {
			try {
				return super.getInputStream(file);
			} catch (IOException e) {
				return null;
			}
		}
	}
	
	/**
	 * プロパティー・ユニットを構築します。
	 */
	public PropertySetUnit() {
	}

	/**
	 * 英語プロパティーを取得します。
	 * <p>
	 * @return 英語プロパティー
	 */
	public PropertySet getEnPropertySet() {
		return enProperties;
	}

	/**
	 * 英語プロパティーをセットします。
	 * <p>
	 * @param enProperties 英語プロパティー
	 */
	public void setEnProperties(PropertySet enProperties) {
		this.enProperties = enProperties;
	}
	
	/**
	 * 日本語プロパティーを取得します。
	 * <p>
	 * @return 日本語プロパティー
	 */
	public PropertySet getJaPropertySet() {
		return jaProperties;
	}

	/**
	 * 日本語プロパティーをセットします。
	 * <p>
	 * @param jaProperties 日本語プロパティー
	 */
	public void setJaProperties(PropertySet jaProperties) {
		this.jaProperties = jaProperties;
	}
}
