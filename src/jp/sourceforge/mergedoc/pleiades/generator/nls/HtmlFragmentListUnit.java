/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.generator.nls;

import java.util.HashMap;
import java.util.Map;

/**
 * 英語と日本語の HTML フラグメントを対で保持するクラスです。
 * <p>
 * @author cypher256
 */
public class HtmlFragmentListUnit {
	
	/** 英語 HTML フラグメント */
	public final Map<String, HtmlFragmentList> enHtmlMap =
		new HashMap<String, HtmlFragmentList>();
	
	/** 日本語 HTML フラグメント */
	public final Map<String, HtmlFragmentList> jaHtmlMap =
		new HashMap<String, HtmlFragmentList>();
}
