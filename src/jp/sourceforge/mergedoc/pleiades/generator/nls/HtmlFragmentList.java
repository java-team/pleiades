/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.generator.nls;

import java.util.LinkedList;

import jp.sourceforge.mergedoc.pleiades.resource.HelpHtmlParser.HtmlFragment;

/**
 * HTML フラグメントのリストです。
 * <p>
 * @author cypher256
 */
@SuppressWarnings("serial")
public class HtmlFragmentList extends LinkedList<HtmlFragment> {

	/** パス (識別用) */
	public final String path;

	/**
	 * HTML フラグメントのリストを構築します。
	 * @param path パス (識別用)
	 */
	public HtmlFragmentList(String path) {
		this.path = path;
	}

	/**
	 * 指定したインデックス位置の要素を取得します。
	 * サイズを超えている場合は null を返します。
	 */
	@Override
	public HtmlFragment get(int index) {
		if (index >= size()) {
			return null;
		}
		return super.get(index);
	}
}
