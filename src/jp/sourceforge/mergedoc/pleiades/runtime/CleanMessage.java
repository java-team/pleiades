/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.runtime;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;

import jp.sourceforge.mergedoc.pleiades.Pleiades;
import jp.sourceforge.mergedoc.pleiades.Pleiades.OS;
import jp.sourceforge.mergedoc.pleiades.log.Logger;
import jp.sourceforge.mergedoc.pleiades.resource.Files;

/**
 * -clean 起動時のメッセージです。
 * @author cypher256
 */
public class CleanMessage {

	/** ロガー */
	private static final Logger log = Logger.getLogger(CleanMessage.class);

	/** ウィンドウ */
	private static JWindow cleanWindow;

	/**
	 * クリーン・メッセージを表示します。
	 * @param splashLocation Eclipse スプラッシュ・ロケーション
	 */
	public static void show(final String splashLocation) {

		// Mac OS Swing デッドロック回避
		if (Pleiades.getOS() == OS.MAC) {
			log.info("クリーン起動します。(Mac ではクリーン・メッセージ表示不可)");
			return;
		}

		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					if (cleanWindow == null) {
						cleanWindow = new JWindow();
					}

					ImageIcon cleanIcon = new ImageIcon(Files.conf("images/splash_clean.png").toURI().toURL());
					Image cleanImage = cleanIcon.getImage();
					Image resizedCleanImage = setupRectangle(splashLocation, cleanImage);
					if (resizedCleanImage != null) {
						cleanIcon.setImage(resizedCleanImage);
					}
					JLabel cleanComponent = new JLabel(cleanIcon);
					cleanWindow.getContentPane().add(cleanComponent);

					cleanWindow.pack();
					cleanWindow.setVisible(true);

					// 邪魔になるため最前面指定はしない
					//window.setAlwaysOnTop(true);

				} catch (Exception e) {
					log.error(e, "クリーン・メッセージの表示に失敗しました。");
				}
			}
		});
	}

	/**
	 * 座標を設定します。
	 * @param splashLocation Eclipse スプラッシュ・ロケーション
	 * @param cleanImage クリーン画像
	 * @return 設定後のクリーン画像
	 */
	protected static Image setupRectangle(String splashLocation, Image cleanImage) throws IOException {

		Dimension eclipseSplash = null;
		if (splashLocation != null) {
			File splashFile = new File(splashLocation);
			if (splashFile.exists()) {
				BufferedImage splash = ImageIO.read(splashFile);
				eclipseSplash = new Dimension(splash.getWidth(), splash.getHeight());
			}
		}

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle cleanRect = cleanWindow.getBounds();
		Image resizedCleanImage = null;

		if (eclipseSplash == null) {

			// Eclipse スプラッシュなしの場合は真ん中に表示
			cleanRect.x = screenSize.width / 2 - cleanImage.getWidth(null) / 2;
			cleanRect.y = screenSize.height / 2 - cleanImage.getHeight(null) / 2;

		} else {

			// Eclipse スプラッシュの横幅と同じになるようにメッセージの横幅を調整
			if (eclipseSplash.width > 400 && eclipseSplash.width < 500) {
				resizedCleanImage = cleanImage.getScaledInstance(eclipseSplash.width, cleanImage.getHeight(null),
						Image.SCALE_SMOOTH);
			}

			// Eclipse スプラッシュのサイズからメッセージの位置を設定
			cleanRect.x = screenSize.width / 2 - (int) Math.ceil(eclipseSplash.width / 2d);
			cleanRect.y = screenSize.height / 2 + eclipseSplash.height / 2;
		}
		cleanWindow.setBounds(cleanRect);

		log.debug("クリーン・メッセージ表示 (Eclipse スプラッシュ:%s, メッセージ:%s)",
			eclipseSplash, cleanRect.getLocation());
		return resizedCleanImage;
	}

	/**
	 * クリーン・メッセージを閉じます。
	 */
	public static void close() {

		// Mac OS Swing デッドロック回避
		if (Pleiades.getOS() == OS.MAC) {
			return;
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				if (cleanWindow != null) {
					cleanWindow.dispose();
					cleanWindow = null;
				}
			}
		});
	}
}
