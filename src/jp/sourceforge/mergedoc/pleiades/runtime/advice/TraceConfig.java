package jp.sourceforge.mergedoc.pleiades.runtime.advice;

import java.util.Map;

import jp.sourceforge.mergedoc.org.apache.commons.lang.StringUtils;
import jp.sourceforge.mergedoc.pleiades.log.Logger;
import jp.sourceforge.mergedoc.pleiades.runtime.resource.RuntimeTranslationDictionary;

/**
 * Pleiades トレース構成クラスです。
 * pleiades-config.xml に定義されているデバッグに関するプロパティーの値を保持しているクラスです。
 * <p>
 * @author cypher256
 */
public class TraceConfig {

	/** ロガー */
	private static final Logger log = Logger.getLogger(RuntimeTranslationDictionary.class);

	/** トレースパターン (未設定時は null) */
	public final String pattern;

	/** トレース対象が日本語 (翻訳後) の場合は true */
	public final boolean isJa;

	/** トレース除外・限定を無効にする場合は true */
	public final boolean disabled;

	/** クラスデスクリプターを出力する場合の完全クラス名 (未設定時は null) */
	public final String logDescClass;

	/**
	 * Pleiades トレース構成を構築します。
	 * @param propertyMap Pleiades 構成ファイルのプロパティーを保持するマップ
	 */
	public TraceConfig(Map<String, String> propertyMap) {

		String _pattern = null;
		boolean _isJa = false;
		boolean _disabled = false;
		String _logDescClass = null;

		if (log.isDebugEnabled()) {

			String pattern = propertyMap.get("trace.pattern");
			if (StringUtils.isNotEmpty(pattern)) {
				_pattern = "(?s)" + pattern; // DOTALL モードプレフィックスを付ける
				_isJa = Boolean.valueOf(propertyMap.get("trace.is.ja"));
				log.warn("翻訳トレース設定が有効 pattern=%s, isJa=%s", _pattern, _isJa);
			}

			_disabled = Boolean.valueOf(propertyMap.get("trace.disabled"));
			_logDescClass = propertyMap.get("log.desc.class");
		}

		pattern = _pattern;
		isJa = _isJa;
		disabled = _disabled;
		logDescClass = _logDescClass;
	}
}
