/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.runtime.advice;

/**
 * 実行時のジョイント・ポイントです。
 * <p>
 * @author cypher256
 */
public class RuntimeJointPoint extends JointPoint {

	/** 対象オブジェクト (Javassist $0) */
	private Object target;

	/** 英語文字列を区切る場合の正規表現 (デフォルト null) */
	public String separator;

	/**
	 * ジョイント・ポイントを構築します。
	 * <p>
	 * @param editPoint 編集ポイント
	 * @param className クラス名
	 * @param methodName メソッド名
	 * @param descriptor メソッド記述子
	 * @param target 対象オブジェクト (Javassist $0)
	 */
	public RuntimeJointPoint(
			EditPoint editPoint,
			String className, String
			methodName,
			String descriptor,
			Object target) {

		super(editPoint, className, methodName, descriptor);
		this.target = target;
	}

	public Object getTarget() {
		return target;
	}
}
