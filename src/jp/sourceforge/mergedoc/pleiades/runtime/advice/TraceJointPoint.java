/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.runtime.advice;

import jp.sourceforge.mergedoc.pleiades.runtime.resource.CallHierarchyExplorer;

/**
 * トレース・ジョイント・ポイント定義です。
 * <p>
 * @author cypher256
 */
public class TraceJointPoint extends JointPoint {

	/** トレース制限数 (0 以下の場合は無制限) */
	private int limit = CallHierarchyExplorer.TRACE_MAX;

	/**
	 * トレース制限数をセットします。
	 * @param limit トレース制限数
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * トレース制限数を取得します。
	 * @return トレース制限数
	 */
	public int getLimit() {
		return limit;
	}
}
