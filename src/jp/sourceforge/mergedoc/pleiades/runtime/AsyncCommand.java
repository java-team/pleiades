/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.runtime;

import java.lang.reflect.InvocationTargetException;

import jp.sourceforge.mergedoc.pleiades.log.Logger;

/**
 * 非同期実行コマンドです。
 * <p>
 * @author cypher256
 */
public abstract class AsyncCommand implements Runnable {

	/** ロガー */
	private static final Logger log = Logger.getLogger(AsyncCommand.class);

	/** コマンド名 */
	private final String commandName;

	/**
	 * コンストラクターです。
	 * @param commandName コマンド名
	 */
	public AsyncCommand(String commandName) {
		this.commandName = commandName;
	}

	/**
	 * コマンド実行テンプレートです。
	 */
	public final void run() {
		try {

			log.debug("非同期実行コマンド実行: " + commandName);
			execute();

		} catch (Throwable e) {
			if (e instanceof InvocationTargetException) {
				e = ((InvocationTargetException) e).getTargetException();
			}
			handleException(e, commandName);
		}
	}

	/**
	 * コマンドを実行します。
	 */
	protected abstract void execute() throws Throwable;

	/**
	 * 例外を処理します。
	 * @param e 例外
	 */
	protected void handleException(Throwable e, String commandName) {
		log.error(e, commandName + "に失敗しました。");
	}
}
