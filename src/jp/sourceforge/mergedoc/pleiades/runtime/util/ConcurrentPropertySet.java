/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.runtime.util;

import java.io.File;

import jp.sourceforge.mergedoc.pleiades.resource.PropertySet;

/**
 * 同期化されたプロパティー・セットです。
 * <ul>
 * <li>マルチスレッド・アクセスによるデッドロック回避のため同期化されています。
 * <li>Map の containsValue メソッドを高速化するために拡張しています。
 * </ul>
 * @author cypher256
 */
@SuppressWarnings("serial")
public class ConcurrentPropertySet extends PropertySet {

	public ConcurrentPropertySet(int initialCapacity) {
		super(initialCapacity);
	}

	public ConcurrentPropertySet(File... files) {
		super(files);
	}

	@Override
	public synchronized String put(String key, String value) {
		return super.put(key, value);
	}

	@Override
	public synchronized String get(Object key) {
		return super.get(key);
	}
}
