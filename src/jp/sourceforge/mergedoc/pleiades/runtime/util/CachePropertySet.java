/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.runtime.util;

import java.io.File;
import java.util.List;

import jp.sourceforge.mergedoc.pleiades.log.Logger;
import jp.sourceforge.mergedoc.pleiades.resource.PropertySet;

/**
 * キャッシュ用のプロパティー・セットです。
 * <p>
 * @author cypher256
 */
@SuppressWarnings("serial")
public class CachePropertySet extends ConcurrentPropertySet {

	/** ロガー */
	private static final Logger log = Logger.getLogger(CachePropertySet.class);

	/** プロパティー保存コメント */
	public final String comment;

	/**
	 * コンストラクタです。
	 * @param initialCapacity 初期容量
	 * @param comment プロパティー保存コメント
	 */
	public CachePropertySet(int initialCapacity, String comment) {
		super(initialCapacity);
		this.comment = comment;
	}

	/**
	 * エントリー数が異なる場合のみ保管します。
	 * <p>
	 * @param file ファイル
	 * @return 保管されたキーのリスト (ソート済み)。保管されなかった場合は null。
	 */
	public List<String> store(File file) {
		return store(file, comment);
	}

	/**
	 * エントリー数が異なる場合のみ保管します。
	 * <p>
	 * @param path パス
	 * @return 保管されたキーのリスト (ソート済み)。保管されなかった場合は null。
	 */
	public List<String> store(String path) {
		return super.store(path, comment);
	}

	@Override
	public List<String> store(File file, String comment) {

		PropertySet old = new PropertySet();
		if (file.exists()) {
			old.load(file);
		}
		List<String> result = null;

		if (size() > old.size()) {
			result = super.store(file, comment);
			log.info(comment + "を更新しました。");
		} else {
			log.info(comment + "の更新はありません。");
		}
		return result;
	}
}
