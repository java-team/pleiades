/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.runtime.util;

import java.util.HashMap;
import java.util.Map;

/**
 * キーだけでなく値もハッシュとして保持するプロパティ・セットです。
 * <ul>
 * <li>containsValue が高速に動作します。
 * </ul>
 * @author cypher256
 */
@SuppressWarnings("serial")
public class ValueHashPropertySet extends CachePropertySet {

	/** 値を高速検索するためのハッシュセット */
	private final Map<String, String> reverseMap = new HashMap<String, String>();

	/**
	 * コンストラクタです。
	 * @param initialCapacity 初期容量
	 * @param comment プロパティー保存コメント
	 */
	public ValueHashPropertySet(int initialCapacity, String comment) {
		super(initialCapacity, comment);
	}

	@Override
	public synchronized boolean containsValue(Object value) {
		return reverseMap.containsKey(value);
	}

	@Override
	public synchronized String put(String key, String value) {
		reverseMap.put(value, key);
		return super.put(key, value);
	}

	/**
	 * 値を指定してキーを取得します。
	 * @param value 値
	 * @return キー
	 */
	public String getKey(String value) {
		return reverseMap.get(value);
	}
}
