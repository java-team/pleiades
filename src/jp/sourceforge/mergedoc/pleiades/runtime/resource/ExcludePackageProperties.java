/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.runtime.resource;

import jp.sourceforge.mergedoc.pleiades.Pleiades;
import jp.sourceforge.mergedoc.pleiades.log.Logger;
import jp.sourceforge.mergedoc.pleiades.resource.Files;
import jp.sourceforge.mergedoc.pleiades.resource.Property;
import jp.sourceforge.mergedoc.pleiades.resource.PropertySet;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static jp.sourceforge.mergedoc.pleiades.resource.FileNames.TRANS_EXCLUDE_PACKAGE_PROP;

/**
 * 翻訳除外パッケージ・プロパティーです。
 * <p>
 * @author cypher256
 */
public class ExcludePackageProperties {

	/** ロガー */
	private static final Logger log = Logger.getLogger(ExcludePackageProperties.class);

	/** このクラスのシングルトン・インスタンス */
	private static final ExcludePackageProperties singleton = new ExcludePackageProperties();

	/**
	 * 翻訳除外パッケージ・プロパティー・オブジェクトを取得します。
	 * <p>
	 * @return 翻訳除外パッケージ・プロパティー・オブジェクト
	 */
	public static ExcludePackageProperties getInstance() {
		return singleton;
	}

	/** 翻訳除外マップ */
	private final Map<String, List<String>> map = new HashMap<String, List<String>>();

	/**
	 * 翻訳除外パッケージ・プロパティーを構築します。
	 */
	private ExcludePackageProperties() {

		String appName = Pleiades.getPleiadesOption().appName;
		if (appName == null) {
			return;
		}
		String appPrefix = appName + ":";

		File file = Files.conf(TRANS_EXCLUDE_PACKAGE_PROP);
		if (!file.exists()) {
			String msg = file.getName() + " が見つかりません。";
			log.fatal(msg);
			Exception e = new FileNotFoundException(file.getPath());
			throw new IllegalStateException(msg, e);
		}

		PropertySet prop = new PropertySet(100).load(file);
		for (Property p : prop) {

			List<String> pkgs = new ArrayList<String>();
			for (String pkg : p.value.split("\\s*,\\s*")) {
				if (pkg.startsWith(appPrefix)) {
					pkgs.add(pkg.substring(appPrefix.length()));
				}
			}
			if (pkgs.size() > 0) {
				map.put(p.key, pkgs);
			}
		}
	}

	/**
	 * 指定した英語リソース文字列から翻訳除外パス・エントリーを取得します。
	 * <p>
	 * @param enValue 英語リソース文字列
	 * @return 翻訳除外パス・エントリー。存在しない場合は null。
	 */
	public List<String> getPathEntries(String enValue) {

		List<String> pathEntries = map.get(enValue);
		if (pathEntries != null && pathEntries.contains("")) {
			log.info(new IllegalStateException(),
					"[" + enValue + "] の翻訳呼び出しスタックトレース。");
			return null;
		}
		return pathEntries;
	}
}
