/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.runtime.resource;

import jp.sourceforge.mergedoc.pleiades.Pleiades;
import jp.sourceforge.mergedoc.pleiades.log.Logger;

/**
 * 実行時のキャッシュ・ファイル定義です。
 * <p>
 * @author cypher256
 */
public class CacheFiles {

	/** ロガー */
	private static final Logger log = Logger.getLogger(CacheFiles.class);

	/** 翻訳キャッシュ・プロパティー・ファイル名 */
	public static String TRANS_CACHE_PROP			= "translation-cache.properties.zip";

	/** ニーモニック変換キャッシュ・プロパティー・ファイル名 */
	public static String MNEMONIC_CACHE_PROP		= "mnemonic-cache.properties.zip";

	/** 除外クラス・リスト・ファイル名 */
	public static String EXCLUDE_CLASS_LIST			= "exclude-class.list";

	/** 変換済みクラス・キャッシュ・ファイル名 */
	public static String TRANSFORMED_CLASS_CACHE	= "transformed-class.cache";

	/**
	 * すべてのキャッシュをクリアします。
	 */
	public static void clear() {
		Pleiades.getResourceFile(TRANS_CACHE_PROP).delete();
		Pleiades.getResourceFile(MNEMONIC_CACHE_PROP).delete();
		Pleiades.getResourceFile(EXCLUDE_CLASS_LIST).delete();
		Pleiades.getResourceFile(TRANSFORMED_CLASS_CACHE).delete();
		log.info("すべてのキャッシュ・ファイルをクリアしました。");
	}
}
