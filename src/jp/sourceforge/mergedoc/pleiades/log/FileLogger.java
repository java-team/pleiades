/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import jp.sourceforge.mergedoc.org.apache.commons.io.IOUtils;
import jp.sourceforge.mergedoc.org.apache.commons.lang.CharEncoding;

/**
 * ファイルに出力するロガーです。
 * <p>
 * @author cypher256
 */
public class FileLogger extends Logger {

	/** ログ・ファイル名を示すシステム・プロパティーのキー */
	public static final String LOG_FILE_NAME = "pleiades.log.file.name";

	/** ファイル PrintStream マップ */
	private static final Map<String, PrintStream> outMap = new HashMap<String, PrintStream>();

	/**
	 * 開かれている出力ストリームをクローズします。
	 */
	public static void close() {

		for (PrintStream out : outMap.values()) {
			IOUtils.closeQuietly(out);
		}
		outMap.clear();
	}

	//-------------------------------------------------------------------------

	/** ログ・ファイル名 */
	protected String logFileName;

	/**
	 * ファイル・ロガーを構築します。
	 * <p>
	 * @param category カテゴリー
	 */
	protected FileLogger(String category) {

		super(category);

		logFileName = System.getProperty(LOG_FILE_NAME);
		if (getOut() != null) {
			return;
		}

		try {
			File logFile = new File(logFileName);
			logFile.getParentFile().mkdirs();
			logFile.delete();
			final PrintStream out = new PrintStream(
					new BufferedOutputStream(new FileOutputStream(logFile)),
					true, CharEncoding.UTF_8);
			outMap.put(logFileName, out);

		} catch (IOException e) {

			throw new RuntimeException("プロパティに指定された " + LOG_FILE_NAME +
					"=" + logFileName + " が不正です。", e);
		}
	}

	/**
	 * ファイル PrintStream を取得します。
	 */
	@Override
	protected PrintStream getOut() {
		return outMap.get(logFileName);
	}

	/**
	 * ファイルとエラー出力にメッセージを出力します。
	 */
	@Override
	public void fatal(Throwable e, String message, Object... args) {
		super.fatal(e, message, args);
		System.err.println(format(message, args));
		if (e != null) System.err.println(e);
	}

	@Override
	public String toString() {
		return super.toString() + " " + logFileName;
	}
}
