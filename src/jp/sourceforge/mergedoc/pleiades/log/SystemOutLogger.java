/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.log;

import java.io.PrintStream;

/**
 * 標準出力に出力するロガーです。
 * <p>
 * @author cypher256
 */
public class SystemOutLogger extends Logger {

	/**
	 * 標準出力ロガーを構築します。
	 * <p>
	 * @param category カテゴリー
	 */
	protected SystemOutLogger(String category) {
		super(category);
	}
	
	/*
	 * (非 Javadoc)
	 * @see Logger#getOut()
	 */
	protected PrintStream getOut() {
		return System.out;
	}
}
