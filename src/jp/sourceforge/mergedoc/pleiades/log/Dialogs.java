/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.log;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import jp.sourceforge.mergedoc.pleiades.Pleiades;
import jp.sourceforge.mergedoc.pleiades.Pleiades.OS;

/**
 * ダイアログ・ユーティリティです。
 * <p>
 * @author cypher256
 */
public class Dialogs {

	/**
	 * 生成できません。
	 */
	private Dialogs() {
	}

	/**
	 * 致命的エラーをポップアップ・メッセージ (Swing) で表示します。
	 */
	public static void fatal(Throwable e, String title, String message, Object... args) {

		try {
			message = String.format(message, args);

			if (e != null) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				message += "\n" + sw;
			}

		} catch (Exception e2) {
			e2.printStackTrace();
		}

		System.err.println(message);
		if (e != null) {
			e.printStackTrace();
		}

		// Mac OS Swing デッドロックバグ回避 https://bugs.eclipse.org/bugs/show_bug.cgi?id=212617
		if (Pleiades.getOS() != OS.MAC) {
			try {
				JOptionPane pane = new JOptionPane(message, JOptionPane.ERROR_MESSAGE);
				JDialog dialog = pane.createDialog(null, title);
				dialog.setAlwaysOnTop(true);
				dialog.setVisible(true);
				dialog.dispose();

			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}
