/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.log;


/**
 * ポップアップ付きのファイル・ロガーです。
 * <p>
 * @author cypher256
 */
public class PopupFileLogger extends FileLogger {

	/**
	 * ポップアップ付きファイル・ロガーを構築します。
	 * <p>
	 * @param category カテゴリー
	 */
	protected PopupFileLogger(String category) {
		super(category);
	}

	/**
	 * ファイルとエラー出力にメッセージを出力します。
	 */
	@Override
	public void error(Throwable e, String message, Object... args) {
		super.error(e, message, args);
		System.err.println(format(message, args));
		if (e != null) System.err.println(e);
	}

	/**
	 * 致命的エラー・ログを出力します。
	 * 通常のログ出力に加え、画面にポップアップ・メッセージ (Swing) を表示します。
	 */
	@Override
	public void fatal(Throwable e, String message, Object... args) {

		// ログ出力
		super.fatal(e, message.replace("\n", ""), args);

		// ポップアップ・メッセージの表示
		message = format(message, args);
		if (e != null) {
			message +=
				"\n原因：" + e + "\n\n" +
				"詳細は以下のログ・ファイルを参照してください。\n" +
				logFileName;
		}
		Dialogs.fatal(e, "Pleiades でエラーが発生しました。", message);
	}
}
