/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades;

import jp.sourceforge.mergedoc.org.apache.commons.lang.StringUtils;
import jp.sourceforge.mergedoc.pleiades.log.Logger;
import jp.sourceforge.mergedoc.pleiades.log.Logger.Level;

/**
 * Pleiades 起動オプションです。
 * <p>
 * @author cypher256
 */
public class PleiadesOption {

	/** Eclipse の -clean (起動時に -clean が指定されている場合は true) */
	public boolean isClean;

	/** ニーモニック非表示 (表示しない場合は true) */
	public boolean isNoMnemonic;

	/** デフォルト・スプラッシュ使用 (使用する場合は true) */
	public final boolean isDefaultSplash;

	/** 辞書に訳が無い場合のログ出力をするか (出力する場合は true) */
	public final boolean enabledNotFoundLog;

	/** ログ・レベル */
	public Level logLevel = Level.INFO;

	/** 初回起動時実行コマンド */
	public String initBatch;

	/** Pleiades 構成ファイル名 */
	public String configXmlFile;

	/** アプリケーション別上書きプロパティーファイル名 (デフォルト null) */
	public String appOverrideProp;

	/** アプリケーション識別名 (デフォルト null) */
	public String appName;

	/**
	 * 起動オプションを構築します。
	 * <p>
	 * @param agentArg エージェント起動引数
	 */
	PleiadesOption(String agentArg) {

		String arg = (agentArg == null) ? "" : "," + agentArg.replace(" ", "") + ",";

		isDefaultSplash = arg.contains(",default.splash,");
		isNoMnemonic = arg.contains(",no.mnemonic,");

		// debug: ログ・レベルと未翻訳ログをまとめて簡略指定 (readme 記載はこのオプション)
		boolean isDebug = arg.contains(",debug,");
		enabledNotFoundLog = isDebug || arg.contains(",enabled.not.found.log,");

		// ログ・レベルの設定
		if (isDebug) {
			logLevel = Level.DEBUG;
		}
		if (arg.contains("log.level=")) {
			String s = arg.replaceFirst("^.*?log\\.level=", "").replaceFirst(",.*$", "");
			try {
				logLevel = Level.valueOf(s.toUpperCase());
			} catch (IllegalArgumentException e) {
				throw new IllegalArgumentException(
					"起動オプション log.level の値 [" + logLevel + "] が不正です。" +
					StringUtils.join(Logger.Level.values(), ",") +
					" のいずれかである必要があります。");
			}
		}

		// 初回起動時実行コマンドの取得
		if (arg.contains("init.batch=")) {
			initBatch = arg.replaceFirst("^.*?init\\.batch\\=", "").replaceFirst(",.*$", "");
		}
	}

	/**
	 * プリロード起動か判定します。
	 * @return プリロード起動の場合は true
	 */
	public boolean isPreload() {
		return System.getProperty("pleiades.preload") != null;
	}
}
