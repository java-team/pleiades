/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.resource;

/**
 * Pleiades で使用するファイル名定義です。
 * <p>
 * @author cypher256
 */
public interface FileNames {

	//-------------------------------------------------------------------------
	// Eclipse 実行時に参照するリソース

	/** デフォルト翻訳プロパティー・ファイル名 */
	String TRANS_PROP						= "translation.properties";

	/** 翻訳優先プロパティー・ファイル名 */
	String TRANS_FIRST_PROP				= "translation-first.properties";

	/** 翻訳正規表現プロパティー・ファイル名 */
	String TRANS_REGEX_PROP				= "translation-regex.properties";

	/** 翻訳スルー正規表現プロパティー・ファイル名 */
	String TRANS_REGEX_THROUGH_PROP		= "translation-regex-through.properties";

	/** 翻訳除外パッケージ・プロパティー・ファイル名 */
	String TRANS_EXCLUDE_PACKAGE_PROP	= "translation-exclude-package.properties";

	/** 翻訳キャッシュ除外プロパティー・ファイル名 */
	String TRANS_CACHE_EXCLUDE_PROP		= "translation-cache-exclude.properties";

	/** 翻訳ヘルプ除外プロパティー・ファイル名 */
	String TRANS_HELP_EXCLUDE_PROP		= "translation-help-exclude.properties";

	/** 翻訳変換プロパティー・ファイル名 */
	String TRANS_CONVERTER_PROP			= "translation-converter.properties";

	/** 翻訳プロパティー追加ディレクトリー */
	String TRANS_ADDITIONS_DIR			= "additions";

	//-------------------------------------------------------------------------
	// 翻訳プロパティー・ジェネレーターが参照するリソース

	/** 校正済み Babel 言語パックプロパティー・ファイル名 */
	String BABEL_CUSTOM_PROP					= "props/source/Eclipse_Babel.properties";

	/** 句点分割なし辞書プロパティー・ファイル名 */
	String NO_SPLIT_PROP						= "props/validation-no-split.properties";

	/** 校正＆ルール適用済み Babel プロパティー・ファイル名 */
	String TEMP_BABEL_CUSTOM_PROP			= "props/temp-Eclipse_Babel.properties";

	/** 辞書全プロパティー（テンポラリー）ファイル名 */
	String TEMP_ALL_PROP						= "props/temp-all.properties";

	/** 言語パックから抽出したヘルプ・プロパティー・ファイル名 (検査済み) */
	String TEMP_NLS_HELP_CHECKED_PROP		= "props/temp-nls-help-checked.properties";

	/** 言語パックから抽出したヘルプ・プロパティー・ファイル名 (未検査) */
	String TEMP_NLS_HELP_UNCHECKED_PROP		= "props/temp-nls-help-unchecked.properties";

	/** 校正済みヘルプ・プロパティー・ファイル名 */
	String HELP_CUSTOM_PROP					= "props/source/Eclipse_Help.properties";

	//-------------------------------------------------------------------------
	// 翻訳バリデーターが参照するリソース

	/** 用語検証プロパティー・ファイル名 */
	String VALID_TERM_PROP					= "props/validation-term.properties";

	/** 翻訳禁止プロパティー・ファイル名 */
	String VALID_FORBIDDEN_TRANS_PROP		= "props/validation-forbidden-translation.properties";

	/** 検証無視プロパティー・ファイル名 */
	String VALID_IGNORE_PROP					= "props/validation-ignore.properties";

	/** 対訳検証プロパティー・ファイル名 */
	String VALID_TRANS_PROP					= "props/validation-translation.properties";

	/** 対訳検証 (逆参照) プロパティー・ファイル名 */
	String VALID_TRANS_REVERSE_PROP			= "props/validation-translation-reverse.properties";

	/** ヘルプ上書きプロパティー・ファイル名 */
	String VALID_HELP_OVERRIDE_PROP			= "props/validation-help-override.properties";

	/** ヘルプ対訳検証プロパティー・ファイル名 */
	String VALID_HELP_TRANS_PROP			= "props/validation-help-translation.properties";

	/** ヘルプ対訳検証プロパティー・ファイル名 */
	String VALID_HELP_TRANS_REVERSE_PROP	= "props/validation-help-translation-reverse.properties";
}
