/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.resource;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import jp.sourceforge.mergedoc.pleiades.log.Logger;

/**
 * 正規表現パターンのキャッシュです。
 * <p>
 * @author cypher256
 */
public class PatternCache {

	/** ロガー */
	private static final Logger log = Logger.getLogger(PatternCache.class);

	/** キャッシュ最大容量 */
	private static int CAPACITY = 1000;

	/** パターン保持する LRU キャッシュ (最大容量に達すると最近最も使われていない要素が削除される) */
	private static final Map<String, Pattern> cache = new LinkedHashMap<String, Pattern>(CAPACITY, 0.75f, true) {

	    @Override
	    protected boolean removeEldestEntry(Map.Entry<String, Pattern> eldest) {
	    	boolean over = size() > CAPACITY;
	    	if (over) {
	    		log.debug("正規表現キャッシュ最大容量 %d 超過のため古い要素を削除しました。%s",
	    			CAPACITY, eldest.getKey());
	    	}
	        return over;
	    }
	};

	/**
	 * キャッシュからパターンを取得します。
	 * 存在しない場合は作成します。
	 * <p>
	 * @param regex 正規表現文字列
	 * @return パターン
	 */
	public static Pattern get(String regex) {

		Pattern pat = cache.get(regex);
		if (pat == null) {
			pat = Pattern.compile(regex);
			cache.put(regex, pat);
		}
		return pat;
	}

	/**
	 * インスタンス化できません。
	 */
	private PatternCache() {
	}
}
