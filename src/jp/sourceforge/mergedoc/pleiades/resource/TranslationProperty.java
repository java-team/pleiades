/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.resource;

/**
 * 1 つの翻訳プロパティーを表すクラスです。
 * <p>
 * @author cypher256
 */
public class TranslationProperty {
	
	/** 英語 */
	public final TranslationString en;
	
	/** 日本語 */
	public final TranslationString ja;
	
	/**
	 * 翻訳プロパティーを構築します。
	 * @param p プロパティー
	 */
	public TranslationProperty(Property p) {
		this(p.key, p.value);
	}
	
	/**
	 * 翻訳プロパティーを構築します。
	 * @param en 英語
	 * @param ja 日本語
	 */
	public TranslationProperty(String en, String ja) {
		this(new TranslationString(en), new TranslationString(ja));
	}
	
	/**
	 * 翻訳プロパティーを構築します。
	 * @param en 英語翻訳文字列
	 * @param ja 日本語翻訳文字列
	 */
	public TranslationProperty(TranslationString en, TranslationString ja) {
		this.en = en;
		this.ja = ja;
	}
	
	/**
	 * プロパティーを取得します。
	 * trim は行われません。
	 * <p>
	 * @return プロパティー
	 */
	public Property getProperty() {
		return new Property(en.toString(), ja.toString());
	}
	
	/**
	 * このオブジェクトの文字列表現を取得します。
	 * @return 文字列表現
	 */
	@Override
	public String toString() {
		return Property.toString(en.toString(), ja.toString());
	}
}
