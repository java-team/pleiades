/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.resource;

import java.io.UnsupportedEncodingException;

/**
 * 文字列ユーティリティーです。
 * <p>
 * @author cypher256
 */
public class Strings {

	/**
	 * インスタンス化できません。
	 */
	private Strings() {
	}

	/**
	 * 文字列をバイト配列に変換します。
	 * @param s 文字列
	 * @param charsetName 文字セット名
	 * @return バイト配列
	 */
	public static byte[] getBytes(String s, String charsetName) {
		try {
			return s.getBytes(charsetName);
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException(e);
		}
	}
}
