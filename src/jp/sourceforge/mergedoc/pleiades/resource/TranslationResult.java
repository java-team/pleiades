/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.resource;

/**
 * 翻訳辞書の結果クラスです。
 * <p>
 * @author cypher256
 */
public class TranslationResult {
	
	/** 翻訳結果文字列 */
	private String value;
	
	/** 辞書有無 */
	private boolean found;

	/**
	 * 翻訳結果文字列を取得します。
	 * @return 翻訳結果文字列
	 */
	public String getValue() {
	    return value;
	}

	/**
	 * 翻訳結果文字列を設定します。
	 * @param value 翻訳結果文字列
	 */
	public void setValue(String value) {
	    this.value = value;
	}

	/**
	 * 辞書有無を取得します。
	 * @return 辞書有無
	 */
	public boolean isFound() {
	    return found;
	}

	/**
	 * 辞書有無を設定します。
	 * @param found 辞書有無
	 */
	public void setFound(boolean found) {
	    this.found = found;
	}
}
