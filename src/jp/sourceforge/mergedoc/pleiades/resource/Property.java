/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.resource;

/**
 * 1 つのプロパティーを表すクラスです。
 * <p>
 * @author cypher256
 */
public class Property {

	/** キー */
	public final String key;

	/** 値 */
	public String value;

	/**
	 * プロパティーを構築します。
	 * @param key
	 * @param value
	 */
	public Property(String key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * このオブジェクトの文字列表現を取得します。
	 * @return 文字列表現
	 */
	@Override
	public String toString() {
		return toString(key, value);
	}

	//-------------------------------------------------------------------------

	/**
	 * 文字列をプロパティーのキー=値の文字列表現に変換します。
	 * <p>
	 * @param key キー
	 * @param value 値
	 * @return プロパティー文字列表現
	 */
	public static String toString(String key, String value) {
		return escapeKey(key) + "=" + escapeValue(value);
	}

	/**
	 * キー文字列をプロパティーのキー（左辺）文字列表現に変換します。
	 * <p>
	 * @param key キー
	 * @return プロパティーのキー文字列表現
	 */
	public static String escapeKey(String key) {
		return key
			.replaceAll("[:=#! \t\\\\]", "\\\\$0")
			.replaceAll("\r", "\\\\r")
			.replaceAll("\n", "\\\\n");
	}

	/**
	 * 値文字列をプロパティーの値（右辺）文字列表現に変換します。
	 * <p>
	 * @param value 値
	 * @return プロパティーの値文字列表現
	 */
	public static String escapeValue(String value) {
		return value
			// 値の [:=] はエスケープしなくても読み込めるため、扱いやすさを優先し対象外
			.replaceAll("[#!]", "\\\\$0")
			.replaceAll("\r", "\\\\r")
			.replaceAll("\n", "\\\\n");
	}
}
