#!/bin/sh

KEY_STORE_FILE=$(cd $(dirname $0) && pwd)/.keystore
rm -f $KEY_STORE_FILE

keytool \
-genkey \
-dname "cn=cypher256, ou=MergeDoc, o=MergeDoc Project, c=JP" \
-keystore $KEY_STORE_FILE \
-alias MergeDoc \
-keyalg RSA \
-keysize 2048 \
-validity 7300 \
-storepass mergedoc \
-keypass mergedoc
