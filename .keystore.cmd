cd /d %~dp0
del .keystore

keytool ^
-genkey ^
-dname "cn=cypher256, ou=MergeDoc, o=MergeDoc Project, c=JP" ^
-keystore .keystore ^
-alias MergeDoc ^
-keyalg RSA ^
-keysize 2048 ^
-validity 7300 ^
-storepass mergedoc ^
-keypass mergedoc
