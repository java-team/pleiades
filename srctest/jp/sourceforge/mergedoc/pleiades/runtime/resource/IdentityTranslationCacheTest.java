package jp.sourceforge.mergedoc.pleiades.runtime.resource;

import junit.framework.TestCase;

public class IdentityTranslationCacheTest extends TestCase {

	protected final IdentityTranslationCache cache = new IdentityTranslationCache();

	protected static final char ESC_CHAR = (char) 0x001B;

	protected void assertLookup(String input, String expected) {
		String result = cache.translateUnderscoreMnemonic(input.replace('@', ESC_CHAR), null);
		System.out.println(input + "=" + result);
		assertEquals(expected, result.replace(ESC_CHAR, '@'));
	}

	protected void assertLookupFalse(String input) {
		String result = cache.translateUnderscoreMnemonic(input, null);
		System.out.println(input + "=" + result);
		assertEquals(input, result);
	}

	// -------------------------------------------------------------------------

	public void testLookup() {

		// ニーモニック、ESC 制御文字 (IDEA 系の一部)
		assertLookup("D@eployment", "デプロイ(@E)");
		assertLookup("@Automatic Upload", "自動アップロード(@A)");

		// ニーモニック、IDEA の特殊なアンパサンド除去
		assertLookup("Include &non-&&project files", "非プロジェクト・ファイルを含める");

		// ニーモニック処理せずに翻訳
		assertLookup("Set layout_width to match_parent", "layout_width に match_parent を設定する");

		// ニーモニック処理しない
		assertLookupFalse("KOTLIN_BUNDLED");
		assertLookupFalse("keep_java-style_getters_and_setters");

		// ニーモニック、シングルクォート内は無視
		assertLookup("'aaa' x_y_z", "'aaa' x_y_z");
		assertLookup("'aaa' x_yz", "'aaa' xyz(_Y)");
		assertLookup("'a_aa' x_yz", "'a_aa' xyz(_Y)");

		// ニーモニック混在 (IDEA メニューで & が消えるため、訳の & を "および" に変更)
		assertLookup("_Tasks & Contexts", "タスクおよびコンテキスト(_T)");
	}
}
