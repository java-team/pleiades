package jp.sourceforge.mergedoc.pleiades.runtime.resource;

import junit.framework.TestCase;

public class RuntimeTranslationDictionaryTest extends TestCase {

	protected final RuntimeTranslationDictionary dynamicDic = new RuntimeTranslationDictionary();

	protected void assertLookup(String input, String expected) {
		{
			String result = dynamicDic.lookup(input, null);
			System.out.println(input + "=" + result);
			assertEquals(expected, result);
		}
	}

	protected void assertLookupFalse(String input) {
		{
			String result = dynamicDic.lookup(input, null);
			System.out.println(input + "=" + result);
			assertEquals(input, result);
		}
	}

	protected void assertLookupFalse(String input, String expected) {
		{
			String result = dynamicDic.lookup(input, null);
			System.out.println(input + "=" + result);
			assertEquals(expected, result);
		}
	}

	// -------------------------------------------------------------------------

	public void testLookup() {

		// [#14691] 文末の空白が除去される問題対応確認
		assertLookup("Sets the language module the spell checking engine should use when you enter a log message. ",
				"ログ・メッセージを入力する時にスペル・チェック・エンジンが使用すべき言語モジュールを設定します。 ");
		// [#14691] 文末のセミコロンが除去される問題対応確認
		assertLookup("assertion failed;", "アサーションに失敗しました。");
		// [#14691] 謎の空白
		assertLookup("{n,} - Greedy match >= n times", "{n,} - 正規表現による一致 n 回以上");

		// [#14699] 末尾かっこ
		assertLookup(
				"Type named ''{0}'' is imported (single-type-import) in ''{1}'' (a compilation unit must not import and declare a type with the same name)",
				"''{0}'' という名前の型は ''{1}'' 内にインポート (単一の型のインポート) されています (コンパイル単位は同じ名前で型をインポートおよび宣言できません)");
		// [#14699] (s)
		assertLookup("Archive file ''{0}'' is referenced by the following project(s). Do you still want to delete it?",
				"アーカイブ・ファイル ''{0}'' は次のプロジェクトによって参照されます。削除しますか?");

		// [#14700] 囲み文字
		assertLookup("\"Malformed file transmission received\"", "\"誤った形式のファイル伝送が受信されました\"");
		// [#14700] (s)
		assertLookup(
				"Problems tagging the resources. {0} project(s) successfully tagged and {1} project(s) with errors.",
				"リソースのタグ付けで問題が発生しました。{0} プロジェクトが正常にタグ付けされ、{1} プロジェクトにエラーがありました。");
		assertLookup("You have chosen to ignore {0} resources. Select what to add to the .cvsignore file(s).",
				"{0} 個のリソースを無視するよう選択しました。.cvsignore ファイルに追加するものを選択してください。");

		// [#14796] 分割後の {1}.
		assertLookupFalse("{0}, Function: {1}.");

		// & を含むパラメーター付き URL は翻訳不可
		assertLookupFalse("/aaa/bbb?xxx=1&yyy=2");

		// [#14799]
		assertLookup("{0} does not exist.\nAre you sure you want to create it?", "{0} は存在しません。\nこれを作成しますか?");

		// [#14801]
		assertLookup("Error copying file {0}: {1}", "ファイル {0} のコピー・エラー: {1}");
		assertLookup("Unable to initialize source location - directory does not exist: {0}",
				"ソース・ロケーションを初期化できません。ディレクトリー {0} がありません");

		// [#14802]
		assertLookup("The selected enterprise bean cannot be deleted because it is involved in\n"
				+ "relationships with other enterprise beans.  You must first delete these\n"
				+ "relationships before deleting the bean.",
				"別のエンタープライズ Bean との関係が含まれるため、\n" + "選択したエンタープライズ Bean は削除できません。\n" + "Bean を削除する前に、まず関係を削除する必要があります。");

		// [#14837]
		assertLookupFalse("The resulting string is too long to fit in a DOMString: ''{0}''.");

		// [#14840]
		assertLookup("Minimum VM Size (kB)", "最小 VM サイズ (kB)");
		assertLookupFalse("kB.");

		// [#14841]
		assertLookup("&Enable annotation roll-over (on new editors)", "(新しいエディター上で) 注釈のロールオーバーを使用可能にする(&E)");
		assertLookup("Unable to locate JAR/zip in file system as specified by the driver definition: {0}.",
				"ドライバー定義で指定されたファイル・システム内に jar/zip が見つかりません: {0}.");

		// [#14842]
		assertLookup("'?' is not expected. '(?:' or '(?=' or '(?!' or '(?<' or '(?#' or '(?>'?",
				"'?' をここに書くことはできません。もしかして '(?:' か '(?=' か '(?!' か '(?<' か '(?#' か '(?>' ですか?");

		// [#14843]
		assertLookup("Invalid timeout! Timeout should be positive integer or -1 to disable.",
				"タイムアウト値が無効です! タイムアウト値は、正整数または -1 (使用不可にする場合) でなければなりません。");

		// [#14844]
		assertLookup("none (no sides, default value)", "none (線なし、デフォルト値)");
		assertLookup("groups (between row groups)", "groups (行グループ間)");

		// [#14845]
		assertLookup(
				"It is strongly recommended you restart {0} for the changes to take effect. For some add-ons, it may be possible to apply the changes you have made without restarting. Would you like to restart now?",
				"変更を反映させるために {0} の再始動を強くお勧めします。いくつかのアドオンでは、再始動しなくても変更が反映されている場合もあります。すぐに再始動しますか?");

		// [#14862]
		assertLookup("Bundle symbolic name contains illegal characters. Legal characters are A-Z a-z 0-9 . _ -",
				"バンドルのシンボル名に正しくない文字が含まれています。正しい文字は A-Z a-z 0-9 . _ - です");

		// [#14865]
		assertLookup("Unable to restore source location - class not found: {0}", "ソース・ロケーションを復元できません。{0} クラスが見つかりません");

		// [#14866]
		assertLookup("Error: File not found.", "エラー: ファイルが見つかりません。");

		// [#14867]
		assertLookup("Update the related faces config file. Are you sure?", "関連する faces 構成ファイルを更新します。よろしいですか?");

		// [#14869]
		assertLookup("<details unavailable - not supported by VM>", "<詳細は使用不可 - VM によってサポートされていません>");

		// [#14870]
		assertLookup(
				"Inclusion or exclusion patterns are disabled in project {1}, cannot selectively include or exclude from entry: ''{0}''",
				"包含または除外パターンはプロジェクト {1} 内で使用不可です。エントリー ''{0}'' から選択的に包含または除外できません");

		// [#14883]
		assertLookup("Profile name is empty.", "プロファイル名が空です。");

		// [#14887]
		assertLookup("An error has occurred: {0}. See error log for more details. Do you want to exit?",
				"エラーが発生しました: {0}。詳細についてはエラー・ログを参照してください。終了しますか?");

		// [#14888]
		assertLookup(
				"Select a set of UDDI businesses and click <strong>Add to Favorites</strong> to add these to the list of favorites. Click <strong>Add to UDDI Page</strong> to add these to the UDDI Page. Click <strong>Refresh</strong> to refresh the selections.",
				"UDDI ビジネスのセットを選択し、「<strong>お気に入りに追加</strong>」をクリックしてこれらのビジネスをお気に入りのリストに追加します。「<strong>UDDI ページに追加</strong>」をクリックしてこれらのビジネスを UDDI ページに追加します。「<strong>リフレッシュ</strong>」をクリックして選択をリフレッシュします。");

		// [#14889]
		assertLookup("-- Java Generation :: Generating...", "-- Java の生成 :: 生成中...");

		// [#14890]
		assertLookup(
				"Inclusion or exclusion patterns are disabled in project {1}, cannot selectively include or exclude from entry: ''{0}''",
				"包含または除外パターンはプロジェクト {1} 内で使用不可です。エントリー ''{0}'' から選択的に包含または除外できません");

		// [#14891]
		assertLookup("CHKJ2017E: This interface must extend javax.ejb.EJBHome (EJB 2.0: 6.3, 7.10.6).",
				"CHKJ2017E: このインターフェースは javax.ejb.EJBHome を拡張しなければなりません (EJB 2.0: 6.3、7.10.6)。");

		// [#14901]
		assertLookup("Warning: Invalid value for the initParam xpoweredBy. Will use the default value of \"false\"",
				"警告: Invalid value for the initParam xpoweredBy. デフォルト値 \"false\" を使用します");
		assertLookup("Problems encountered during code generation. Select detail entries for more information.",
				"コード生成中に問題が発生しました。Select detail entries for more information.");
		assertLookup(
				"No custom build file found in {0}. If you want to generate a new build file, temporarily disable the ''custom'' property. ",
				"{0} でカスタム・ビルド・ファイルが検出されませんでした。If you want to generate a new build file, temporarily disable the ''custom'' property. ");

		// [#14902]
		assertLookup("Missing directory entry: {0}.", "欠落しているディレクトリー・エントリー: {0}.");
		assertLookup("Missing directory entry: {1}.", "欠落しているディレクトリー・エントリー: {1}.");

		// [#14944]
		assertLookup("IO exception.", "IO 例外。");
		assertLookup("IO exception", "IO 例外");

		// [#14946]
		assertLookup(
				"When selecting {0} compliance, make sure to have a compatible JRE installed and activated (Currently {1}). "
						+ "Configure the <a href=\"1\" >Installed JREs</a> or change the JRE on the <a href=\"2\" >Java build path</a>.",

				"{0} 準拠を選択する場合は、互換性がある JRE がインストール済みでアクティブにされていることを確認してください (現在は {1})。"
						+ "<a href=\"1\" >インストール済み JRE</a> を構成するか、<a href=\"2\" >Java ビルド・パス</a> にある JRE を変更してください。");

		// [#14947]
		assertLookup("Package: Generate Javadoc for all but private classes and members.",
				"Package: private のクラスおよびメンバー以外について Javadoc を生成します。");
		assertLookup("Package: Generate Jsdoc for all but private classes and members.",
				"Package: private のクラスおよびメンバー以外について Jsdoc を生成します。");

		// [#14954]
		assertLookupFalse("visibility scope where Javadoc comments are not checked.");
		assertLookupFalse("A type.");

		// [#14955]
		assertLookup("n/a", "なし");
		assertLookup("N/A", "なし");

		// [#15186]
		assertLookupFalse("C&VS", "CVS(&V)");

		// [#15193]
		assertLookupFalse("Policy on how to pad parentheses. nospace = do not pad (e.g. method(a, b)). space = ensure padding (e.g. method( a, b )).");

		// [#15277]
		assertLookup(
				"Project Validator that validates the Spring Config files (e.g. class and property names, referenced beans etc.)",
				"Spring 構成ファイルを検証するプロジェクト・バリデーター (例 クラスとプロパティーの名前、 参照される bean など)");

		// -----------------------------------------------------------------------------------------

		// ノーマル
		assertLookup("Label", "ラベル");

		// ノーマル false
		assertLookupFalse("... undefined string in dictionary ...");

		// ノーマル、文
		assertLookup("Error reading configuration", "構成の読み取り中にエラーが発生しました");

		// アクセラレーター
		assertLookup("Add &Include@Ctrl+Shift+N", "Include の追加(&I)@Ctrl+Shift+N");

		// ニーモニックとアクセラレーター
		assertLookup("Go to &Line...@Ctrl+L", "指定行へジャンプ(&L)...@Ctrl+L");

		// ニーモニックではない &
		assertLookup("A resource named \"&1\" already exists.", "リソース \"&1\" はすでに存在します。");
		assertLookup("K&R [built-in]", "K&R [ビルトイン]");
		assertLookup("Appearance & Behavior", "外観 & 振る舞い");
		assertLookup("Tasks & Contexts", "タスクおよびコンテキスト");

		// ニーモニックではない &&
		assertLookup("Ignore '&&' in &Java properties files", "Java プロパティー・ファイルの '&&' を無視(&J)");

		// ニーモニック
		assertLookup("P&ackage", "パッケージ(&A)");

		// ニーモニック (再翻訳)
		assertLookup("再ビルド(&E) モジュール 'Pleiades'", "モジュールの再ビルド(&E) 'Pleiades'");

		// ニーモニック 先頭 1 文字 + ドットは ja 変換しない
		assertLookup("&A. サムネイル", "&A. サムネイル");

		// ニーモニック
		assertLookup("L&abel", "ラベル(&A)");

		// ニーモニック、先頭
		assertLookup("&Left", "左(&L)");

		// ニーモニック 翻訳済み -> ニーモニックがある場合はそのまま
		assertLookup("左(&L)", "左(&L)");

		// ニーモニック、文
		assertLookup("Remo&ve Buildfile", "ビルドファイルの除去(&V)");

		// 正規表現
		assertLookup("JavaHL 1.4.5 Win32 Binaries (Optional)", "JavaHL 1.4.5 Win32 バイナリー (オプション)");

		// 正規表現に日本語が含まれる場合のテスト
		assertLookup("Filter 列", "列のフィルター");
		assertLookup("Filter 列 ...", "列のフィルター ...");

		// 正規表現プロパティーの Loading (\S.) が優先され "ロード中 files" にならないこと
		assertLookup("Loading files", "ファイルのロード時");

		// 末尾 :
		assertLookup("Search results for {0}:", "{0} の検索結果:");

		// 末尾 ...
		assertLookup("&Extension....", "拡張(&E)....");

		// 末尾 ...
		assertLookup("Label...", "ラベル...");

		// 末尾 省略の .
		assertLookup("sec.", "秒");

		// 末尾 >
		assertLookup("&Next >", "次へ(&N) >");

		// 末尾 >>
		assertLookup("&Next >>", "次へ(&N) >>");

		// 末尾 -->
		assertLookup("&Add -->", "追加(&A) -->");

		// 末尾空白
		assertLookup("Label ", "ラベル ");

		// 末尾タブ
		assertLookup("Label\t", "ラベル\t");

		// 末尾 (Incubation)
		assertLookup("Label (Incubation)", "ラベル (インキュベーション)");

		// 末尾 (Experimental)
		assertLookup("Label [Experimental]", "ラベル [実験用]");

		// 先頭 <<
		assertLookup("<< &Previous", "<< 前へ(&P)");

		// a タグ
		assertLookup("&Using {0} - <a>Select other...</a>", "{0} を使用中 - <a>その他の選択(&U)...</a>");

		// HTML タグ囲み 1 つ
		assertLookup("<html>Previous</html>", "<html>前へ</html>");

		// HTML タグ囲み複数
		assertLookup("<html><font color=gray>Previous</font></html>", "<html><font color=gray>前へ</font></html>");

		// HTML 文字実体参照
		assertLookup("&nbsp;&nbsp;Label&nbsp;&nbsp;", "&nbsp;&nbsp;ラベル&nbsp;&nbsp;");

		// HTML 参照文字 (&productName;)
		assertLookup(
				"<!DOCTYPE html>\n<html>\n<head>\n    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/tips.css\">\n</head>\n<body>\n\n\n      <p>Did you know that you can close tabs in the editor and the tool windows of &productName; without actually\n          using the context menu commands?\n      It is enough to point with your mouse cursor to a tab to be closed, and click the middle mouse button, or just\n      use the <span class=\"shortcut\">Shift+click</span> combination.</p>\n\n\n</body>\n</html>",
				"<!DOCTYPE html><html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"css/tips.css\"></head><body><p>コンテキスト・メニューのコマンドを実際に使用しなくても &productName; のエディターとツール・ウィンドウのタブを閉じることができることをご存知ですか? 閉じたいタブにマウス・カーソルを合わせてマウスの中ボタンをクリックするか <span class=\"shortcut\">Shift+click</span> の組み合わせを使用するだけで十分です。</p></body></html>");

		// 先頭空白
		assertLookup(" Label", " ラベル");

		// 先頭タブ
		assertLookup("\tLabel", "\tラベル");

		// 複数形 (s)
		assertLookup("Delete Bookmark", "ブックマークの削除");
		assertLookup("Delete Bookmark(s)", "ブックマークの削除");

		// ピリオド + 連続する英小文字 (翻訳しない)
		assertLookupFalse(".test");

		// 句点分割 改行
		assertLookup(
				"Add, remove, or edit installed Web browsers.\n"
						+ "The selected Web browser will be used by default when Web pages are opened, "
						+ "although some applications may always use the external browser.",

				"インストール済み Web ブラウザーを追加、除去、または編集します。\n"
						+ "選択された Web ブラウザーは、Web ページが開いているときにデフォルトで使用されますが、"
						+ "一部のアプリケーションでは、常に外部ブラウザーが使用されます。");

		// 句点分割 改行と末尾改行 1 つ
		assertLookup(
				"Filter files may be used to include or exclude bug detection for particular classes and methods.\n<a href=\"http://findbugs.sourceforge.net/manual/filter.html\">Details...</a>\n",
				"フィルター・ファイルは特定のクラスやメソッドのバグ検出を含めたり除外したりするために使用します。\n<a href=\"http://findbugs.sourceforge.net/manual/filter.html\">詳細...</a>\n");

		// 句点分割 改行と末尾改行 2 つ以上
		assertLookup(
				"MySQL driver files are missing.\nDBeaver can download these files automatically.\n\n",
				"MySQL ドライバー・ファイルが欠落しています。\nDBeaver は自動的にこれらのファイルをダウンロードすることができます。\n\n");

		// 句点分割 ()
		assertLookup("{0} must be public. (EJB 1.1: 9.2.3). ", "{0} は public でなければなりません。(EJB 1.1: 9.2.3)。 ");
		assertLookup("Detected charset %s (Current setting: %s)", "検出された文字セット %s (現在の設定: %s)");

		// 句点分割 () 2 つ
		assertLookup("Subversive SVN JDT Ignore Extensions Sources (Optional) (Incubation)",
				"Subversive SVN JDT 無視拡張ソース (オプション) (インキュベーション)");

		// 句点分割 () + :
		assertLookup("Full user name (first and last):", "完全ユーザー名 (姓名):");

		// 句点分割 文字を示す -
		assertLookup(
				"Performs a line-by-line comparison of all code lines and reports duplicate code if a sequence of lines differs only in indentation. All import statements in Java code are ignored, any other line - including javadoc, whitespace lines between methods, etc. - is considered (which is why the check is called strict).",
				"すべてのコード行の行単位比較を実行し、行シーケンスのインデントだけが異なる場合、重複したコードを報告します。Java コード内のすべてのインポート・ステートメントは無視され、その他の行 - javadoc、 メソッド間の空白行などを含む - は考慮されます (このチェックが厳密と呼ばれる理由)。");

		// 句点分割 1桁の後のドットは。に置換しない
		assertLookup("1. Project", "1. プロジェクト");

		// nbsp 空白
		assertLookup("Marketplace Client\u00A0(Sources)", "マーケットプレース・クライアント\u00A0(ソース)");

		// ... とかっこ
		assertLookup("File Properties... (UTF-8)", "ファイル・プロパティー... (UTF-8)");

		// 強制トリム
		assertLookup("Invalid input url:", "無効な URL 入力:");
		assertLookup("Invalid input url", "無効な URL 入力");

		// 全角混在 (記号表記は AOP で変更可能にしたため訳から除外)
		assertLookup("Space ( ・ )", "空白");
		assertLookup("Ideographic space ( ° )", "全角空白");
		assertLookup("Tab ( ≫ )", "タブ");
		assertLookup("Carriage Return ( \u00a4 )", "復帰");
		assertLookup("Line Feed ( ¶ )", "改行");
		assertLookup("Convert Line Delimiters to Windows (CRLF, \\r\\n, 0D0A, \u00A4¶)",
				"行区切り文字を Windows に変換 (CRLF, \\r\\n, 0D0A, \u00A4¶)");

		//「参照 updates」になってしまう ⇒ 正規表現を修正
		assertLookup("Browsing for updates", "更新の参照");
		assertLookup("Browsing for xxx.xxx", "参照 xxx.xxx");
		assertLookupFalse("Browsing for xxx");

		// 辞書にエントリーがあっても末尾 . が 。 になる ⇒ ジェネレータの強制トリム廃止
		assertLookup("Misc.", "その他");

		// 末尾 . → 。変換なし (. 無しキャッシュがあると 。になる不具合確認)
		assertLookupFalse("com.xxxxx");
		assertLookupFalse("com.xxxxx.");

		// 末尾 。が付いてしまう
		assertLookup("To go to the next frame, or toolbar press Ctrl+Tab", "次のフレームまたはツールバーにジャンプするには、Ctrl+TAB を押してください");

		// Tasks が翻訳されない ⇒ 正規表現を否定先読みに修正
		assertLookup("Show Project Tasks", "プロジェクト・タスクの表示");
		assertLookup("Show Project aaa", "プロジェクト aaa の表示");

		// 正規表現取得は分割前 ⇒ 正規表現で記述する
		assertLookup(
				"Displays the configuration model of root project 'a'. [incubating]",
				"ルート・プロジェクト 'a' の構成モデルを表示します。");
		assertLookup(
				"Displays the configuration model of root project 'a'.",
				"ルート・プロジェクト 'a' の構成モデルを表示します。");
		assertLookup(
				"The file size (9666013 bytes) exceeds configured limit (2560000 bytes). Code insight features are not available.",
				"ファイル・サイズ (9666013 バイト) が設定済みの制限 (2560000 バイト) を超えています。コード・インサイト機能は使用できません。");

		// デフォルトは "拡張" とし、%IF% プレフィックスによるパッケージ条件で解決する
		assertLookup("Extensions", "拡張");

		// 拡張子を表す . が 。に誤変換されないか確認
		assertLookup("Label.<img src=\"a.png\">", "ラベル。<img src=\"a.png\">");
		assertLookup("Label<img src=\"a.png\">" , "ラベル<img src=\"a.png\">");

		// $ を含む HTML 参照
		assertLookup(
				"<p>&s</p><p>Then return to your source code and press <span class=\"shortcut\">&shortcut:$Paste;</span></p>",
				"<p>&s</p><p>その後、ソース・コードに戻り <span class=\"shortcut\">&shortcut:$Paste;</span> を押します</p>");

		// span タグがある場合は句点分割されない (IDEA tips)
		assertLookup(
				"You don't need to invoke quick documentation explicitly (<span class=\"shortcut\">&shortcut:QuickJavaDoc;</span>)",
				"クイック・ドキュメントを明示的に呼び出す必要はありません (<span class=\"shortcut\">&shortcut:QuickJavaDoc;</span>)");
		assertLookup(
				"<html> <head>     <link rel=\"stylesheet\" type=\"text/css\" href=\"css/tips.css\"> </head> <body>  <p>You don't need to invoke quick documentation explicitly  (<span class=\"shortcut\">&shortcut:QuickJavaDoc;</span>) - if you just move your mouse  pointer over the desired symbol, the quick documentation pop-up window will show automatically.</p> <p>To enable this feature, select the check box <span class=\"control\">Show quick doc on mouse move</span>  in the editor settings. </p> </body> </html>",
				"<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"css/tips.css\"></head><body><p>クイック・ドキュメントを明示的に呼び出す必要はありません (<span class=\"shortcut\">&shortcut:QuickJavaDoc;</span>) - マウス・ポインターを目的のシンボルの上に移動するだけで、クイック・ドキュメントのポップアップ・ウィンドウが自動的に表示されます。</p><p>この機能を有効にするには、エディター設定で <span class=\"control\">マウス移動でクイック・ドキュメントを表示する</span> チェック・ボックスをオンにします。</p></body></html>");

		// ?s などの正規表現は翻訳しない
		assertLookupFalse("?s");

		// 先頭 !
		assertLookup("add entries prefixed with !", "! を前に付けたエントリーを追加");

		// 正規表現、先頭タグは除去して正規表現定義
		assertLookup(
				"<div style=\"text-align:right\" vertical-align=\"top\">'テキスト→折り返しテキスト'<br>of <a href=\"テキスト//折り返しテキスト\">一般",
				"<div style=\"text-align:right\" vertical-align=\"top\">'テキスト→折り返しテキスト'<br><a href=\"テキスト//折り返しテキスト\">一般");

		// 分割後の囲み文字
		assertLookup("Context: <no context>", "コンテキスト: <コンテキストなし>");

		// 分割後の括弧トリム後の再帰分割
		assertLookup("Label. (Note: Status)", "ラベル。(注: 状況)");

		// !!! が trimForce で例外発生しないか確認
		assertLookup("Zen HTML: !!! (<!doctype html>)", "Zen HTML: !!! (<!doctype html>)");
	}
}
