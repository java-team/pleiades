/*
 * Copyright (c) 2005- Shinji Kashihara.
 * All rights reserved. This program are made available under
 * the terms of the Eclipse Public License v1.0 which accompanies
 * this distribution, and is available at epl-v10.html.
 */
package jp.sourceforge.mergedoc.pleiades.generator;

import jp.sourceforge.mergedoc.pleiades.resource.Property;
import jp.sourceforge.mergedoc.pleiades.resource.PropertySet;
import junit.framework.TestCase;

/**
 * テスト・クラスです。
 * <p>
 * @author cypher256
 */
public class TranslationRuleTest extends TestCase {

	/** 翻訳ルール */
	private TranslationRule rule = new TranslationRule();

	/** 翻訳ルール適用 */
	private PropertySet apply(PropertySet inProp) {

		for (Property p : inProp) {
			System.out.println(p);
		}
		PropertySet outProp = rule.apply(inProp);
		for (Property p : outProp) {
			System.out.println(p);
		}
		System.out.println();
		return outProp;
	}

	/** テスト */
	public void testApply() {

		// 句点分割 。
		PropertySet inProp = new PropertySet();
		inProp.put("abcde. xyz.  ", "あいうえお。んんん。 ");
		PropertySet outProp = apply(inProp);
		assertEquals("あいうえお。", outProp.get("abcde."));
		assertEquals("んんん。", outProp.get("xyz."));

		// 句点分割 -
		inProp = new PropertySet();
		inProp.put(
			"Dynamic Languages Toolkit - Core Frameworks",
			"動的言語ツールキット - コア・フレームワーク");
		outProp = apply(inProp);
		assertEquals("動的言語ツールキット", outProp.get("Dynamic Languages Toolkit"));
		assertEquals("コア・フレームワーク", outProp.get("Core Frameworks"));

		// 句点分割 末尾括弧
		inProp = new PropertySet();
		inProp.put(
			"{0} must be public. (EJB 1.1: 9.2.3).",
			"{0} は public でなければなりません。(EJB 1.1: 9.2.3)。");
		outProp = apply(inProp);
		assertEquals(
			"{0} は public でなければなりません。",
			outProp.get("{0} must be public."));

		// 句点分割 末尾括弧 2 つ
		inProp = new PropertySet();
		inProp.put(
			"CHKJ2412I: The return type must be serializable at runtime.  (EJB 1.1: 6.10) (RMI 1.3: 2.6).",
			"CHKJ2412I: 戻りの型は実行時にシリアライズ可能でなければなりません。 (EJB 1.1: 6.10) (RMI 1.3: 2.6)。");
		outProp = apply(inProp);
		assertEquals(1, outProp.size());
		assertEquals(
			"戻りの型は実行時にシリアライズ可能でなければなりません。",
			outProp.get("The return type must be serializable at runtime."));

		// 句点分割 かっこ
		inProp = new PropertySet();
		inProp.put(
			"CHKJ2408E: {0} must be public. (EJB 1.1: 9.2.4, 9.2.5)",
			"CHKJ2408E: {0} は public でなければなりません。 (EJB 1.1: 9.2.4、9.2.5)");
		outProp = apply(inProp);
		assertEquals(2, outProp.size());
		assertEquals("{0} は public でなければなりません。", outProp.get("{0} must be public."));
		assertEquals("EJB 1.1: 9.2.4、9.2.5", outProp.get("EJB 1.1: 9.2.4, 9.2.5"));

		// 句点分割 かっこ + .
		inProp = new PropertySet();
		inProp.put(
			"CHKJ2408E: {0} must be public. (EJB 1.1: 9.2.4, 9.2.5).",
			"CHKJ2408E: {0} は public でなければなりません。 (EJB 1.1: 9.2.4、9.2.5)。");
		outProp = apply(inProp);
		assertEquals(2, outProp.size());
		assertEquals("{0} は public でなければなりません。", outProp.get("{0} must be public."));
		assertEquals("EJB 1.1: 9.2.4、9.2.5", outProp.get("EJB 1.1: 9.2.4, 9.2.5"));

		// 句点分割 かっこ + :
		inProp = new PropertySet();
		inProp.put(
			"Full user name (first and last):",
			"完全ユーザー名 (姓名):");
		outProp = apply(inProp);
		assertEquals(2, outProp.size());
		assertEquals("完全ユーザー名", outProp.get("Full user name"));
		assertEquals("(姓名):", outProp.get("(first and last):"));

		// 句点分割 (K&R は翻訳されないため null)
		inProp = new PropertySet();
		inProp.put(
			"K&R [built-in]",
			"K&R [ビルトイン]");
		outProp = apply(inProp);
		assertEquals(null, outProp.get("K&R"));
		assertEquals("ビルトイン", outProp.get("built-in"));

		// 句点分割 対訳チェックで不正
		inProp = new PropertySet();
		inProp.put(
			"Returns the percentage rank of a value.Return value is in the range [0..1]",
			"値の割合 (%) の順序を返します。戻り値は範囲 [0..1] です");
		outProp = apply(inProp);
		assertEquals(1, outProp.size());

		// トリム Eclipse エラーコード
		inProp = new PropertySet();
		inProp.put(
			"IWAE0005E IWAJ0131I Cannot add the feature to the key because the feature is null for the command:",
			"IWAE0005E IWAJ0131I はコマンドのフィーチャーが null であるため、キーにフィーチャーを追加できません :");
		outProp = apply(inProp);
		assertEquals(
			"IWAJ0131I はコマンドのフィーチャーが null であるため、キーにフィーチャーを追加できません:",
			outProp.get("IWAJ0131I Cannot add the feature to the key because the feature is null for the command:"));

		// 置換
		inProp = new PropertySet();
		inProp.put(
			"x.",
			"DOM AST Javadoc ノードは、構造体タグ・エレメントを持たずに、単なるフラット・テキストになります。");
		outProp = apply(inProp);
		assertEquals(
			"DOM AST Javadoc ノードは、構造体タグ要素を持たずに、単なるフラット・テキストになります。",
			outProp.get("x."));

		// 置換
		inProp = new PropertySet();
		inProp.put(
			"x.",
			"自動タスク・タグと平行して、\nこのリストは、 コンパイラーによって出されるタスク・マーカーの優先順位 (高、中、低) を定義します。 ");
		outProp = apply(inProp);
		assertEquals(
			"自動タスク・タグと平行して、\nこのリストは、 コンパイラーによって出されるタスク・マーカーの優先度 (高、中、低) を定義します。",
			outProp.get("x."));

		// 置換 (改行があるため句点分割されない)
		inProp = new PropertySet();
		inProp.put(
			"Create a project for an existing web application.\n" +
			"Select the directory that contains the project files, and enter the project name.",
			"既存の web アプリケーションのためのプロジェクトを作成します。\n" +
			"プロジェクト・ファイルを含むディレクトリを選択し、プロジェクト名を入力してください。");
		outProp = apply(inProp);
		assertEquals(
			"既存の web アプリケーションのためのプロジェクトを作成します。\n" +
			"プロジェクト・ファイルを含むディレクトリーを選択し、プロジェクト名を入力してください。",
			outProp.get(
				"Create a project for an existing web application.\n" +
				"Select the directory that contains the project files, and enter the project name."));

		// [#14869] VM が 2 個出現
		inProp = new PropertySet();
		inProp.put(
			"<details unavailable - not supported by VM>",
			"<詳細は使用不可 - VM によってサポートされていません>");
		outProp = apply(inProp);
		assertEquals("詳細は使用不可", outProp.get("details unavailable"));
		assertEquals("VM によってサポートされていません", outProp.get("not supported by VM"));
	}
}
