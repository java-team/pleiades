package jp.sourceforge.mergedoc.pleiades.resource;

import junit.framework.TestCase;

/**
 * テスト・クラスです。
 * <p>
 * @author cypher256
 */
public class FilesTest extends TestCase {
	
	/** テスト */
	public void test() {
		
		assertEquals(".b_b.txt", Files.toVcIgnoreName("b_b.txt"));
		assertEquals("aaa/.b_b.txt", Files.toVcIgnoreName("aaa/b_b.txt"));
	}
}
