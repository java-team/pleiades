package jp.sourceforge.mergedoc.pleiades.resource;

import junit.framework.TestCase;

/**
 * テスト・クラスです。
 * <p>
 * @author cypher256
 */
public class MnemonicsTest extends TestCase {

	/** ニーモニックを英語から日本語に変換 */
	private String convertMnemonicEnToJa(String enWithMnemonic, String en, String ja) {
		String s = Mnemonics.convertEnToJa(enWithMnemonic, en, ja);
		System.out.println(enWithMnemonic + "\n" + s + "\n");
		return s;
	}
	
	/** テスト */
	public void testConvertMnemonicEnToJa() {
		
		String s = null;
		
		s = convertMnemonicEnToJa("&hoge @Ctrl+X", "hoge @Ctrl+X", "ほげ @Ctrl+X");
		assertEquals("ほげ (&H)@Ctrl+X", s);
		
		s = convertMnemonicEnToJa("'@&Override'", "'@Override'", "'@Override'");
		assertEquals("'@Override'(&O)", s);

		// ニーモニックとアクセラレーターではない @
		s = convertMnemonicEnToJa("set&Up/@Before", "setUp/@Before", "setUp/@Before");
		assertEquals("setUp/@Before(&U)", s);
	}

	/** テスト */
	public void testRemoveEnMnemonic() {
		
		String enValue = "Ignore '&&' in &Java properties files";
		String enNoMnemonic = Mnemonics.removeEn(enValue);
		assertEquals("Ignore '&&' in Java properties files", enNoMnemonic);
	}
}
